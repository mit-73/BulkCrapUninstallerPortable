# BulkCrapUninstallerPortable
#### Remove large amounts of unwanted applications quickly.
Bulk Crap Uninstaller (or BCUninstaller) is a free (as in speech) program uninstaller. It excels at removing large amounts of applications with minimal user input. It can clean up leftovers, detect orphaned applications, run uninstallers according to premade lists, and much more! Even though BCU was made with IT pros in mind, by default it is so straight-forward that anyone can use it.

## How to use it:
1) Download and unpack the archive on the way \PatchToPortableApps\
2) Create an "BulkCrapUninstaller" folder in the path \PatchToPortableApps\BulkCrapUninstallerPortable\App\
3) Download [Bulk Crap Uninstaller](https://github.com/Klocman/Bulk-Crap-Uninstaller) from the official website, and move \PatchToPortableApps\BulkCrapUninstallerPortable\App\BulkCrapUninstaller\

